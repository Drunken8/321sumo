﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public SpriteRenderer m_SpriteRendererTutorial;

    // Start is called before the first frame update
    void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
        //Thats hacky but better than having unlimited ~2.000 fps IMHO
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown && !Input.GetKeyDown(KeyCode.JoystickButton2))
        {
            SceneManager.LoadScene(1);
        }
        else if (Input.GetKeyDown(KeyCode.JoystickButton2))
        {
            m_SpriteRendererTutorial.enabled = true;
        }
        else if (Input.GetKeyUp(KeyCode.JoystickButton2))
        {
            m_SpriteRendererTutorial.enabled = false;
        }
    }
}
