﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    //player Properties
    public int m_iPlayerID = 1;
    public PlayerState m_ePlayerState;

    //Keybinds
    private string m_sPlayerAxis;
    private KeyCode m_kPlayerMashing;
    private KeyCode m_kPlayerLTrap;
    private KeyCode m_kPlayerRTrap;
    private KeyCode m_kPlayerReady;
    private KeyCode m_kPlayerShoot;


    [Header("PlayerPhysic")]
    public float m_fDashSpeed = 5000.0f;
    public float m_fStaminaRegen = 10.0f;
    public float m_fDashingCost = 100.0f;
    public float m_fShootCost = 50.0f;
    public float m_fHealth = 100.0f;
    public float m_fStamina = 100.0f;
    public Vector2 m_v2HitDir = new Vector2(1, 0);
    public Vector3 m_v3ShootDir = new Vector3(1, 0,0);


    [Header("External Objects")]
    public SpriteRenderer m_spriteRenderer;
    public GameObject m_goSlimeFloor;
    public Rigidbody2D m_rigidbody;
    public GameObject m_goTarget;
    public GameObject m_goSlimeBall;

    public GameObject m_goSliderHP;
    public GameObject m_goSliderMP;

    [Header("Mashing Properties")]
    public int m_iMashingBase = 20;
    public int m_iReadyModifier = -3;
    public int m_iSlimedModifier = +3;
    public int m_iDashingModifier = -3;
    public int m_iMyStaminaModifier = +3;

    [Header("Sprites")]
    public Sprite m_spriteDash1;
    public Sprite m_spriteDash2;
    public Sprite m_spriteHit;
    public Sprite m_spriteReady;
    public Sprite m_spriteStandard;

    [Header("Charge Bar")]
    public Sprite[] m_aChargeStates;
    public SpriteRenderer m_spriteRendererBar;

    [Header("Traps")]
    public GameObject[] m_aTraps;

    [HideInInspector]
    public float m_fDashDir = 0;
    [HideInInspector]
    public int m_iMashCounter = 0;
    [HideInInspector]
    public int m_iMashingGoal = 0;
    public enum PlayerState
    {
        Idle,
        Dashing,
        Mashing,
        Hit,
        Ready,
        Slimed
    };


    // Start is called before the first frame update
    void Start()
    {
        updateState(PlayerState.Idle);
    }

    private void Awake()
    {
        //NOTE: kinda dirty to do this in awake and not in a proper Class configurable from a menu
        //but is only a quick fix to make it playable without controller...

        if (Input.GetJoystickNames().Length >= m_iPlayerID && Input.GetJoystickNames()[m_iPlayerID -1].Contains("XBOX 360"))
        {
            m_sPlayerAxis = $"Player{m_iPlayerID}";
            m_kPlayerMashing = KeyCode.JoystickButton0 + 20 * m_iPlayerID;
            m_kPlayerLTrap = KeyCode.JoystickButton4 + 20 * m_iPlayerID;
            m_kPlayerRTrap = KeyCode.JoystickButton5 + 20 * m_iPlayerID;
            m_kPlayerReady = KeyCode.JoystickButton1 + 20 * m_iPlayerID;
            m_kPlayerShoot = KeyCode.JoystickButton3 + 20 * m_iPlayerID;

            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            Debug.Log($"Player {m_iPlayerID}: Using Controller");
                Debug.Log(Input.GetJoystickNames()[m_iPlayerID-1]);
            #endif
        }
        else
        {
            m_sPlayerAxis = $"Player{m_iPlayerID}Key";

            if (m_iPlayerID == 1)
            {
                //use left part of keyboard
                m_kPlayerMashing = KeyCode.Space;
                m_kPlayerLTrap = KeyCode.Q;
                m_kPlayerRTrap = KeyCode.E;
                m_kPlayerReady = KeyCode.LeftShift;
                m_kPlayerShoot = KeyCode.C;
            } else
            {
                //use right part of keyboard
                m_kPlayerMashing = KeyCode.KeypadEnter;
                m_kPlayerLTrap = KeyCode.Keypad2;
                m_kPlayerRTrap = KeyCode.Keypad3;
                m_kPlayerReady = KeyCode.RightControl;
                m_kPlayerShoot = KeyCode.Keypad0;
            }

            #if UNITY_EDITOR || DEVELOPMENT_BUILD
                Debug.Log($"Player {m_iPlayerID}: Using Keyboard");
            #endif
        }
    }

    private IEnumerator ieIdle()
    {
        m_spriteRenderer.sprite = m_spriteStandard;
        m_rigidbody.velocity = Vector2.zero;
        while (m_ePlayerState == PlayerState.Idle)
        {
            updateStamina(m_fStaminaRegen * Time.deltaTime);
            m_fDashDir = Input.GetAxisRaw(m_sPlayerAxis);
            if (Mathf.Abs(m_fDashDir) > 0.05f)
            {
                if (m_fStamina > m_fDashingCost * Time.deltaTime)
                {
                    updateState(PlayerState.Dashing);
                }
                else
                {
                    setStamina(0);
                }
            }

            handleShoot();
            handleReady();
            handleAttack();
            yield return null;
        }
    }


    private IEnumerator ieDashingAnimation()
    {
        int bPos = 0;
        while (m_ePlayerState == PlayerState.Dashing)
        {
            m_spriteRenderer.sprite = bPos == 0 ? m_spriteDash1 : m_spriteDash2;
            bPos = (bPos + 1) % 2;
            yield return new WaitForSeconds(0.05f);
        }
    }


    private IEnumerator ieDashing()
    {
        StartCoroutine(ieDashingAnimation());
        while (m_ePlayerState == PlayerState.Dashing)
        {
            m_fDashDir = Input.GetAxisRaw(m_sPlayerAxis);
            updateStamina(-m_fDashingCost * Time.deltaTime);

            if (m_fStamina <= 0 || Mathf.Abs(m_fDashDir) <= 0.05f)
            {
                updateState(PlayerState.Idle);
            }
            else
            {
                m_rigidbody.AddForce(Vector2.right * m_fDashDir * m_fDashSpeed * Time.deltaTime);
            }

            handleReady();
            handleAttack();
            yield return null;
        }
    }

    private IEnumerator ieMashing()
    {
        m_spriteRenderer.sprite = m_spriteDash2;
        setChargeBar(0);

        while (m_ePlayerState == PlayerState.Mashing)
        {
            bool bMash = Input.GetKeyDown(m_kPlayerMashing);
            if (bMash)
            {
                m_iMashCounter++;
                setChargeBar((float)m_iMashCounter / m_iMashingGoal);
            }

            if (m_iMashCounter >= m_iMashingGoal)
            {
                m_goTarget.GetComponent<Player>().getHit();
                updateState(PlayerState.Idle);
            }
            yield return null;
        }

        setChargeBar(-1);
    }

    private IEnumerator ieHit()
    {
        m_spriteRenderer.sprite = m_spriteHit;
        updateStamina(m_fStaminaRegen * Time.deltaTime);
        m_rigidbody.AddForce(m_v2HitDir * 1000); //TODO: strength?
        yield return null;
        //Exits, State is changed if ground is hit
    }

    private IEnumerator ieReady()
    {

        m_spriteRenderer.sprite = m_spriteReady;
        m_rigidbody.velocity = Vector2.zero;
        while (m_ePlayerState == PlayerState.Ready)
        {
            if (Input.GetKeyUp(m_kPlayerReady))
            {
                updateState(PlayerState.Idle);
            }
            yield return null;
        }
    }

    private IEnumerator ieSlimed()
    {
        //TODO: spawn particles to differentiate
        m_spriteRenderer.sprite = m_spriteStandard;
        m_spriteRenderer.color = new Color(0.8f, 0.26f, 1,1);
        m_goSlimeFloor.SetActive(true);
        
        yield return new WaitForSeconds(2);

        //cleanup
        m_spriteRenderer.color = Color.white;
        m_goSlimeFloor.SetActive(false);
        if (m_ePlayerState == PlayerState.Slimed)
        {
            updateState(PlayerState.Idle);
        }
    }

    // Update is called once per frame
    void Update()
    {
        handleTraps(); //Traps can be activated independent of PlayerState
    }

    private void updateStamina(float _fAmount)
    {
        m_fStamina = Mathf.Clamp(m_fStamina + _fAmount, 0, 100.0f);
        m_goSliderMP.transform.localScale = new Vector3(m_fStamina / 100.0f, 1, 1);
    }

    private void setStamina(float _fAmount)
    {
        m_fStamina = Mathf.Clamp(_fAmount, 0, 100.0f);
        m_goSliderMP.transform.localScale = new Vector3(m_fStamina / 100.0f, 1, 1);
    }

    private void updateHealth(float _fAmount)
    {
        m_fHealth = Mathf.Clamp(m_fHealth + _fAmount, 0, 100.0f);
        m_goSliderHP.transform.localScale = new Vector3(m_fHealth / 100.0f, 1, 1);

        if (m_fHealth <= 0) // I Lost
        {
            Object.FindObjectOfType<FightManager>().endFight(m_iPlayerID == 1 ? 2 : 1);
        }
    }

    private void handleAttack()
    {

        if (Input.GetKeyDown(m_kPlayerMashing) && m_goTarget != null && m_goTarget.GetComponent<Player>().m_ePlayerState != PlayerState.Hit)
        {
            m_goTarget.GetComponent<Player>().startMashing();
            this.startMashing();
        }
    }

    private void handleReady()
    {

        if (Input.GetKeyDown(m_kPlayerReady))
        {
            updateState(PlayerState.Ready);
        }
    }

    private void handleTraps()
    {

        if (Input.GetKeyDown(m_kPlayerRTrap)) //Right bumper
        {
            m_aTraps[0].GetComponent<ITrap>().bActivateTrap();
        }
        else if (Input.GetKeyDown(m_kPlayerLTrap)) //left bumper
        {
            m_aTraps[1].GetComponent<ITrap>().bActivateTrap();
        }
    }


    private void handleShoot()
    {
        if (m_fStamina < m_fShootCost)
        {
            return;
        }

        if (Input.GetKeyDown(m_kPlayerShoot))
        {
            updateStamina(-m_fShootCost);
            GameObject goSlimeInstance =  Instantiate(m_goSlimeBall, this.transform.position + m_v3ShootDir*3, Quaternion.identity);
            goSlimeInstance.GetComponent<Rigidbody2D>().velocity = m_v3ShootDir*40;
        }
    }

    public void startMashing()
    {

        //apply modifiers
        m_iMashingGoal = m_iMashingBase
            + (m_ePlayerState == PlayerState.Dashing ? m_iDashingModifier : 0)
            + (m_ePlayerState == PlayerState.Ready ? m_iReadyModifier : 0)
            + (m_ePlayerState == PlayerState.Slimed ? m_iSlimedModifier : 0)
            + (int)(m_fStamina/100.0f * m_iMyStaminaModifier);

        #if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.Log($"Player {m_iPlayerID}: {m_iMashingGoal}");
        #endif 

        m_iMashCounter = 0;
        updateState(PlayerState.Mashing);
    }

    public void getHit()
    {
        updateHealth(m_iMashCounter - m_iMashingGoal); //get dmg based only on your non-archivement
        updateState(PlayerState.Hit);
    }

    public void getHitByTrap()
    {
        updateHealth(-10); //get Flat dmg
        updateState(PlayerState.Hit);
    }

    private void updateState(PlayerState _eState)
    {
        switch (_eState)
        {
            case PlayerState.Idle:
                m_ePlayerState = _eState;
                StartCoroutine(ieIdle());
                break;
            case PlayerState.Dashing:
                if (m_fStamina > m_fDashingCost * Time.deltaTime)
                {
                    m_ePlayerState = _eState;
                    StartCoroutine(ieDashing());
                }
                else
                {
                    setStamina(0);
                }
                break;
            case PlayerState.Mashing:
                m_ePlayerState = _eState;
                StartCoroutine(ieMashing());
                break;
            case PlayerState.Hit:
                m_ePlayerState = _eState;
                StartCoroutine(ieHit());
                break;
            case PlayerState.Ready:
                m_ePlayerState = _eState;
                StartCoroutine(ieReady());
                break;
            case PlayerState.Slimed:
                m_ePlayerState = _eState;
                StartCoroutine(ieSlimed());
                break;
            default:
                break;
        }

        #if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.Log(m_ePlayerState);
        #endif
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (m_ePlayerState == PlayerState.Hit && collision.gameObject.tag.Contains("Wall"))
        {
            updateState(PlayerState.Idle);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>())
        {
            m_goTarget = collision.gameObject;
        }
        else if (collision.tag.Contains("Slime"))
        {
            if (m_ePlayerState != PlayerState.Ready)
            {
                updateState(PlayerState.Slimed);
                Destroy(collision.gameObject);
            } else
            {
                Destroy(collision.gameObject);
                //TODO: reflect slimeball
            }
        }
        else if (collision.tag.Contains("Trap"))
        {
            getHitByTrap();
        }
    }

    private void setChargeBar(float _fChargePercent)
    {
        if (_fChargePercent < 0)
        {
            m_spriteRendererBar.enabled = false;
        } else
        {
            m_spriteRendererBar.enabled = true;
            m_spriteRendererBar.sprite = m_aChargeStates[(int)(_fChargePercent* (m_aChargeStates.Length-0.5f))];
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>())
        {
            m_goTarget = null;
        }
    }
}
