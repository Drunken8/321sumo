﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VentilatorTrap : MonoBehaviour, ITrap
{

    public GameObject m_goTriggerVolume;
    public float m_fCooldown = 30.0f;
    public float m_fRespawn = 2.0f;
    public Vector3 m_v3ResetPosition;
    public GameObject m_goVentilator;

    [HideInInspector]
    private Animator m_animator;
    [HideInInspector]
    private bool m_bIsRespawning = true;

    protected void Awake()
    {
        m_animator = m_goVentilator.GetComponent<Animator>();
    }

    public bool bActivateTrap()
    {
        if (m_bIsRespawning)
        {
            return false;
        } else
        {
            m_animator.SetTrigger("trigActivate");
            m_goTriggerVolume.SetActive(true);
            return true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        m_v3ResetPosition = m_goVentilator.transform.position;
        StartCoroutine(ieSpawning());
    }

    private IEnumerator ieSpawning()
    {

        m_goTriggerVolume.SetActive(false);
        m_bIsRespawning = true;
        m_goVentilator.GetComponent<Rigidbody2D>().simulated = false;
        m_goVentilator.transform.position = m_v3ResetPosition;
        //was destroyed, start respawn
        yield return new WaitForSeconds(m_fRespawn);
        m_animator.SetTrigger("trigSpawned");
        yield return new WaitForSeconds(m_fCooldown);
        m_animator.SetTrigger("trigIsReady");
        //End, wait for userInput
        m_bIsRespawning = false;
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        m_animator.SetTrigger("trigDestroyed");
        StartCoroutine(ieSpawning());
    }
}
