﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FightManager : MonoBehaviour
{
    public Text m_TextTimer;
    public Text m_TextCountdown;
    public int m_iMaxTime;

    public Player m_Player1;
    public Player m_Player2;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ieCountDown());
    }

    private IEnumerator ieTimer()
    {
        for (int i = m_iMaxTime; i > 0; i--)
        {
            m_TextTimer.text =  $"{i}";
            yield return new WaitForSeconds(1);
        }

        //END THE GAME, more HP wins
        endFight(m_Player1.m_fHealth > m_Player2.m_fHealth ? 1 : 2);
    }

    private IEnumerator ieCountDown()
    {
        m_TextCountdown.enabled = true;

        yield return new WaitForSeconds(1);

        m_TextCountdown.color = new Color(1, 0.5f, 0.1f);
        m_TextCountdown.text = "2";
        yield return new WaitForSeconds(1);

        m_TextCountdown.color = new Color(1, 0.3f, 0.1f);
        m_TextCountdown.text = "1";
        yield return new WaitForSeconds(1);

        m_TextCountdown.enabled = false;

        StartCoroutine(ieTimer());
        m_Player1.enabled = true;
        m_Player2.enabled = true;
    }

    public void endFight(int _iWinner)
    {

        m_TextCountdown.enabled = true;
        StopAllCoroutines();

        //Disable players + input
        m_Player1.StopAllCoroutines();
        m_Player2.StopAllCoroutines();

        m_TextCountdown.color = Color.black;
        m_TextCountdown.text = $"Player {_iWinner} Wins!";
        StartCoroutine(ieWaitForEndToLoad());
    }

    private IEnumerator ieWaitForEndToLoad()
    {
        yield return new WaitForSeconds(1); //wait a bit that message is not skipped

        for(; ; ) { 
            if (Input.anyKeyDown)
            {
                SceneManager.LoadScene(0);
            }
            yield return null;
        }
    }
}
