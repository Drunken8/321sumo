﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripodTrap : MonoBehaviour, ITrap
{

    public GameObject m_goTriggerVolume;
    public float m_fCooldown = 31.75f;
    public float m_fRespawn = 0.25f;
    public GameObject m_goTripod;
    public GameObject m_goExplosion;

    [HideInInspector]
    private Animator m_animatorTripod;
    [HideInInspector]
    private Animator m_animatorExplosion;
    [HideInInspector]
    private bool m_bIsRespawning = true;

    protected void Awake()
    {

        m_animatorTripod = m_goTripod.GetComponent<Animator>();
        m_animatorExplosion = m_goExplosion.GetComponent<Animator>();
    }

    public bool bActivateTrap()
    {
        if (m_bIsRespawning)
        {
            return false;
        }
        else
        {
            m_goTriggerVolume.SetActive(true);
            m_animatorExplosion.SetTrigger("trigExplode");
            StartCoroutine(ieReload());
            return true;
        }
    }

    private IEnumerator ieReload()
    {
        m_bIsRespawning = true;
        yield return new WaitForSeconds(m_fRespawn);
        m_animatorTripod.SetBool("bIsReady", false);
        m_goExplosion.SetActive( false);
        m_goTriggerVolume.SetActive(false);
        yield return new WaitForSeconds(m_fCooldown);
        m_animatorTripod.SetBool("bIsReady", true);
        m_goExplosion.SetActive(true);
        m_bIsRespawning = false;
    }
    

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ieReload());
    }
    
    
}
